import * as types from '../constants';

const initialState = {
  search: {
    searchQuery: null,
    loading: false,
    error: false,
    response: false,
    message: null,
    movies: [],
    total: 0,
    currentPagination: 1,
    paginationVisible: null,
  },
  movie: {
    movieID: null,
    loading: false,
    error: false,
    content: {},
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.RESET_MOVIES:
      return {
        ...state,
        search: {
          ...state.search,
          movies: [],
          currentPagination: 1,
        },
      };

    case types.UPDATE_PAGINATION_COUNTER:
      return {
        ...state,
        search: {
          ...state.search,
          currentPagination: state.search.currentPagination + 1,
        },
      };
    case types.FETCH_SEARCH_DATA:
      return {
        ...state,
        search: {
          ...state.search,
          searchQuery: action.payload,
          currentPagination: 1,
        },
      };
    case types.FETCH_SEARCH_DATA_BEGIN:
      return {
        ...state,
        search: {
          ...state.search,
          loading: true,
        },
      };
    case types.FETCH_SEARCH_DATA_SUCCESS:
      return {
        ...state,
        search: {
          ...state.search,
          message: action.payload.message,
          response: action.payload.response,
          loading: false,
          error: false,
          movies: [...state.search.movies, ...action.payload.movies],
          total: action.payload.total,
          paginationVisible:
            state.search.movies.length + action.payload.movies.length <
            action.payload.total,
        },
      };
    case types.FETCH_SEARCH_DATA_FAILURE:
      return {
        ...state,
        search: {
          ...state.search,
          loading: false,
          error: action.payload,
        },
      };

    case types.FETCH_MOVIE_DATA:
      return {
        ...state,
        movie: {
          ...state.movie,
          movieID: action.payload,
          error: false,
          content: {},
        },
      };
    case types.FETCH_MOVIE_DATA_BEGIN:
      return {
        ...state,
        movie: {
          ...state.movie,
          loading: true,
        },
      };
    case types.FETCH_MOVIE_DATA_SUCCESS:
      return {
        ...state,
        movie: {
          ...state.movie,
          loading: false,
          error: false,
          content: action.payload.content,
        },
      };
    case types.FETCH_MOVIE_DATA_FAILURE:
      return {
        ...state,
        movie: {
          ...state.movie,
          loading: false,
          error: action.payload,
          content: {},
        },
      };

    default:
      return state;
  }
};
