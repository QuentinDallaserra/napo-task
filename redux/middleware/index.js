import * as types from '../constants';
import {
  getSearchData,
  loadMoreSearchData,
  getMovieData,
} from '../../api';

export default store => next => action => {
  switch (action.type) {
    case types.FETCH_SEARCH_DATA:
      getSearchData(store, action);
      return next(action);
    case types.LOAD_MORE:
      loadMoreSearchData(store, action);
      return next(action);
    case types.FETCH_MOVIE_DATA:
      getMovieData(store, action);
      return next(action);
    default:
      return next(action);
  }
};
