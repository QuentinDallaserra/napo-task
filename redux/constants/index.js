export const LOAD_MORE = 'LOAD_MORE';
export const SEARCH_QUERY = 'SEARCH_QUERY';
export const RESET_MOVIES = 'RESET_MOVIES';
export const UPDATE_PAGINATION_COUNTER = 'UPDATE_PAGINATION_COUNTER';
export const FETCH_SEARCH_DATA = 'FETCH_SEARCH_DATA';
export const FETCH_SEARCH_DATA_BEGIN = 'FETCH_SEARCH_DATA_BEGIN';
export const FETCH_SEARCH_DATA_SUCCESS = 'FETCH_SEARCH_DATA_SUCCESS';
export const FETCH_SEARCH_DATA_FAILURE = 'FETCH_SEARCH_DATA_FAILURE';

export const FETCH_MOVIE_DATA = 'FETCH_MOVIE_DATA';
export const FETCH_MOVIE_DATA_BEGIN = 'FETCH_MOVIE_DATA_BEGIN';
export const FETCH_MOVIE_DATA_SUCCESS = 'FETCH_MOVIE_DATA_SUCCESS';
export const FETCH_MOVIE_DATA_FAILURE = 'FETCH_MOVIE_DATA_FAILURE';
