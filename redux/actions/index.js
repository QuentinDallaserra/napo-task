import * as types from '../constants';

export const loadMore = () => dispatch => {
  dispatch({
    type: types.LOAD_MORE,
  });
};

export const resetMovies = () => dispatch => {
  dispatch({
    type: types.RESET_MOVIES,
  });
};

export const updatePaginationCounter = () => dispatch => {
  dispatch({
    type: types.UPDATE_PAGINATION_COUNTER,
  });
};

export const fetchSearchData = searchQuery => dispatch => {
  dispatch({
    type: types.FETCH_SEARCH_DATA,
    payload: searchQuery,
  });
};

export const fetchSearchDataBegin = () => dispatch => {
  dispatch({
    type: types.FETCH_SEARCH_DATA_BEGIN,
  });
};

export const fetchSearchDataSuccess = data => dispatch => {
  dispatch({
    type: types.FETCH_SEARCH_DATA_SUCCESS,
    payload: data,
  });
};

export const fetchSearchDataFailure = data => dispatch => {
  dispatch({
    type: types.FETCH_SEARCH_DATA_FAILURE,
    payload: data,
  });
};

export const fetchMovieData = id => dispatch => {
  dispatch({
    type: types.FETCH_MOVIE_DATA,
    payload: id,
  });
};

export const fetchMovieDataBegin = () => dispatch => {
  dispatch({
    type: types.FETCH_MOVIE_DATA_BEGIN,
  });
};

export const fetchMovieDataSuccess = data => dispatch => {
  dispatch({
    type: types.FETCH_MOVIE_DATA_SUCCESS,
    payload: data,
  });
};

export const fetchMovieDataFailure = data => dispatch => {
  dispatch({
    type: types.FETCH_MOVIE_DATA_FAILURE,
    payload: data,
  });
};
