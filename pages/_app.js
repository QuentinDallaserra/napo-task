import { createGlobalStyle, ThemeProvider } from 'styled-components';
import { connect } from 'react-redux';
import { library, config } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import '@fortawesome/fontawesome-svg-core/styles.css';
import theme from '../styles/theme';
import resets from '../styles/resets';
import wrapper from '../redux/store';
import Base from '../components/base/base';

config.autoAddCss = false;
library.add(fab, fas);

const GlobalStyle = createGlobalStyle`
  ${resets}
`;

function App({ Component, pageProps }) {
  return (
    <>
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <Base>
          <Component {...pageProps} />
        </Base>
      </ThemeProvider>
    </>
  );
}

export default wrapper.withRedux(connect(null, null)(App));
