import { connect } from 'react-redux';
import Search from '../components/pages/home/search';
import Snippet from '../components/pages/home/snippet';
import Movies from '../components/pages/home/movies';
import Loading from '../components/global/loading';
import ErrorMessage from '../components/pages/home/error-message';
import data from '../api/static-data.js';

const mapStateToProps = state => ({
  movies: state.search.movies,
  response: state.search.response,
  message: state.search.message,
  searchQuery: state.search.searchQuery,
  loading: state.search.loading,
});

function Home({ movies, response, message, searchQuery, loading }) {
  return (
    <>
      <Search />
      {!searchQuery && <Snippet content={data.snippet} />}
      {searchQuery && !response && !loading && (
        <ErrorMessage content={message} />
      )}
      {searchQuery && loading && <Loading />}
      {movies.length > 0 && response && <Movies movies={movies} />}
    </>
  );
}

export default connect(mapStateToProps, null)(Home);
