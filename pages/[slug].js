import { connect } from 'react-redux';
import { useEffect } from 'react';
import { fetchMovieData } from '../redux/actions';
import MovieContent from '../components/pages/single/movie';
import { lastUrlSegment } from '../utils';

const mapStateToProps = state => ({
  content: state.movie.content,
  loading: state.movie.loading,
});

const mapDispatchToProps = {
  fetchMovieData,
};

function Movie({ fetchMovieData, content, loading }) {
  useEffect(() => {
    fetchMovieData(lastUrlSegment());
  }, []);

  return (
    <>
      <MovieContent
        poster={content.Poster}
        rating={content.imdbRating}
        title={content.Title}
        plot={content.Plot}
        released={content.Released}
        loading={loading}
      ></MovieContent>
    </>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Movie);
