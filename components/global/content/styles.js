import styled from 'styled-components';

export const StyledContent = styled.div`
  max-width: 1000px;
  margin-left: auto;
  margin-right: auto;
  padding: 0 30px;
`;
