import { StyledContent } from './styles';

export default function Content({ children }) {
  return <StyledContent>{children}</StyledContent>;
}
