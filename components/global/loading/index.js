import { StyledLoading } from './styles';
import data from '../../../api/static-data.js';

export default function Loading() {
  return <StyledLoading>{data.loadingLabel}</StyledLoading>;
}
