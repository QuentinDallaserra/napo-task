import { Poster } from './styles';
import { useEffect, useState } from 'react';
import data from '../../../api/static-data.js';
import { imageExists } from '../../../api';

export default function Movie({ src, title }) {
  const [imageIsAvailable, setImageIsAvailable] = useState(false);

  useEffect(() => {
    imageExists(src, setImageIsAvailable);
  });

  return (
    <Poster
      src={
        ((src === data.NA || !src || !imageIsAvailable) && data.posterImage) ||
        src
      }
      alt={title}
    />
  );
}
