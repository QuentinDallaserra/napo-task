import styled from 'styled-components';

export const Poster = styled.img`
  width: 100%;
  margin-bottom: 10px;
  margin-top: 0;
`;
