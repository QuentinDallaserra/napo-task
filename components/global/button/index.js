import { StyledButton } from './styles';

export default function Button({ label, onClick, disabled }) {
  return <StyledButton onClick={onClick} disabled={disabled}>{label}</StyledButton>;
}
