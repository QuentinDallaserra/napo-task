import styled from 'styled-components';

export const StyledButton = styled.button`
  margin-left: auto;
  margin-right: auto;
  display: block;
  margin-bottom: 50px;
  border: solid 1px;
  border-radius: 20px;
  padding: 10px 20px;
  background-color: ${({ theme }) => theme.colors.transparent};
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 16px;
  &:hover {
    background-color: ${({ theme }) => theme.colors.black};
    color: ${({ theme }) => theme.colors.white};
  }
  ${props => props.disabled && `
    opacity: 0.9;
    pointer-events: none;
  `}
`;
