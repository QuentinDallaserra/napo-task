import styled from 'styled-components';

export const StyledTitle = styled.h1`
  max-width: 1000px;
  margin: 0 auto 30px;
  padding: 50px 30px 0 30px;
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 40px;
`;
