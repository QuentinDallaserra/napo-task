import Head from 'next/head';

export default function HeadSite() {
  return (
    <Head>
      <meta charSet="utf-8" />
      <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=1"
      />
      <meta name="mobile-web-app-capable" content="yes" />
      <meta name="apple-mobile-wep-app-capable" content="yes" />
    </Head>
  );
}
