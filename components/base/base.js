import HeadSite from './head';
import Title from '../../components/global/title';
import Content from '../../components/global/content';
import data from '../../api/static-data.js';

export default function Base({ children }) {
  return (
    <>
      <HeadSite />
      <Title title={data.pageTitle} />
      <Content>{children}</Content>
    </>
  );
}
