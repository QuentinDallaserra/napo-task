import { StyledErrorMessage } from './styles';

export default function ErrorMessage({ content }) {
  return <StyledErrorMessage>{content}</StyledErrorMessage>;
}
