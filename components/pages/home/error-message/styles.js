import styled from 'styled-components';

export const StyledErrorMessage = styled.div`
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 18px;
  text-align: center;
  padding: 30px 0;
`;
