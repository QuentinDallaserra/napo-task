import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const StyledSearch = styled.div`
  border: solid 1px;
  padding: 10px;
  margin-bottom: 30px;
  display: flex;
  align-items: center;
  ${props =>
    props.disabled &&
    `
    opacity: 0.4;
    pointer-events: none;
  `}
`;
export const Icon = styled(FontAwesomeIcon)`
  font-size: 20px;
  margin-right: 10px;
`;
export const Input = styled.input.attrs({
  type: 'text',
})`
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 18px;
  &:disabled {
    background-color: ${({ theme }) => theme.colors.transparent};
  }
`;
