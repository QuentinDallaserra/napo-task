import { connect } from 'react-redux';
import { StyledSearch, Icon, Input } from './styles';
import { fetchSearchData } from '../../../../redux/actions';
import data from '../../../../api/static-data.js';
import { useEffect, useRef } from 'react';

const mapStateToProps = state => ({
  initialQuery: state.search.searchQuery,
  loading: state.search.loading,
});

const mapDispatchToProps = {
  fetchSearchData,
};

function Search({ initialQuery, fetchSearchData, loading }) {
  const searchInput = useRef(null);
  let debounceTimeout;

  useEffect(() => {
    !loading && searchInput.current.focus({ preventScroll: true });
  });

  const handleInput = string => {
    clearTimeout(debounceTimeout);
    debounceTimeout = setTimeout(() => {
      fetchSearchData(string);
    }, 500);
  };

  return (
    <StyledSearch disabled={loading}>
      <Icon icon={['fas', 'fa-search']} />
      <Input
        ref={searchInput}
        disabled={loading}
        onChange={event => handleInput(event.target.value)}
        placeholder={data.searchMoviePlaceholder}
        defaultValue={initialQuery}
      />
    </StyledSearch>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Search);
