import { connect } from 'react-redux';
import { StyledMovies, Container } from './styles';
import Movie from '../movie';
import Button from '../../../global/button';
import { removeDuplicateByKey } from '../../../../utils';
import { loadMore } from '../../../../redux/actions';
import data from '../../../../api/static-data.js';

const mapStateToProps = state => ({
  paginationVisible: state.search.paginationVisible,
  loading: state.search.loading,
  searchQuery: state.search.searchQuery,
});

const mapDispatchToProps = {
  loadMore,
};

function Movies({ movies, loadMore, paginationVisible, loading }) {
  return (
    <StyledMovies>
      <Container>
        {removeDuplicateByKey(movies, 'imdbID').map(movie => (
          <Movie
            key={movie.imdbID}
            id={movie.imdbID}
            title={movie?.Title}
            poster={movie?.Poster}
            loading={loading}
          />
        ))}
      </Container>
      {paginationVisible && (
        <Button
          onClick={loadMore}
          label={(loading && data.loadingLabel) || data.buttonLabel}
          disabled={loading}
        />
      )}
    </StyledMovies>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Movies);
