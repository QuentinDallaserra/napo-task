import styled from 'styled-components';

export const StyledMovies = styled.div``;

export const Container = styled.div`
  display: grid;
  align-items: center;
  justify-content: center;
  grid-gap: 40px;
  grid-template-columns: repeat(3, 1fr);
  margin-bottom: 50px;
`;
