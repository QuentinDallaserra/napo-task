import { connect } from 'react-redux';
import { fetchMovieData } from '../../../../redux/actions';
import { useRouter } from 'next/router';
import { StyledMovie, Title } from './styles';
import data from '../../../../api/static-data.js';
import Poster from '../../../global/poster';

const mapDispatchToProps = {
  fetchMovieData,
};

function Movie({ poster, title, id, fetchMovieData }) {
  const router = useRouter();

  const handleRoute = id => {
    fetchMovieData(id);
    router.push(id);
  };
  
  return (
    <StyledMovie onClick={() => handleRoute(id)}>
      <Poster src={poster} alt={title} />
      <Title>{title || data.missingTitle}</Title>
    </StyledMovie>
  );
}

export default connect(null, mapDispatchToProps)(Movie);
