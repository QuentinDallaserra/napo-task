import styled from 'styled-components';
import { UserSelect } from '../../../../styles/mixins';

export const StyledMovie = styled.a`
  display: flex;
  flex-direction: column;
  align-items: space-between;
  justify-content: center;
  height: 100%;
  ${UserSelect}
`;

export const Title = styled.h2`
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 20px;
  line-height: 1.2em;
  font-weight: 300;
  text-align: center;
  margin-top: auto;
`;
