import styled from 'styled-components';

export const StyledSnippet = styled.div`
  font-family: ${({ theme }) => theme.fonts.arial};
  font-size: 18px;
  line-height: 1.2em;
`;
