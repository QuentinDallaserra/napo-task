import { StyledSnippet } from './styles';

export default function Snippet({ content }) {
  return <StyledSnippet>{content}</StyledSnippet>;
}
