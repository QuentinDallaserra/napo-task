import {
  StyledMovie,
  Content,
  Rating,
  StarRating,
  Title,
  Plot,
  Released,
} from './styles';
import data from '../../../../api/static-data.js';
import Loading from '../../../global/loading';
import Poster from '../../../global/poster';

export default function Movie({
  poster,
  rating,
  title,
  released,
  plot,
  loading,
}) {
  const date = new Date(released).getFullYear();

  return (
    <StyledMovie>
      <Poster src={poster} alt={title} />
      {(loading && <Loading />) || (
        <Content>
          <Title>{title}</Title>
          <Rating>
            {(rating == data.NA && data.noRating) || (
              <StarRating percentage={rating * 10} />
            )}
          </Rating>

          <Plot>{plot}</Plot>
          {released !== data.NA && (
            <Released>
              {data.releasedIn}
              {date}
            </Released>
          )}
        </Content>
      )}
    </StyledMovie>
  );
}
