import styled from 'styled-components';

export const StyledMovie = styled.div`
  display: grid;
  grid-template-columns: 3fr 5fr;
  font-family: ${({ theme }) => theme.fonts.arial};
`;
export const Content = styled.div`
  padding-left: 40px;
`;
export const Rating = styled.div`
  margin-bottom: 20px;
`;
export const StarRating = styled.div`
  font-size: 30px;
  height: 30px;
  width: 124px;
  position: relative;
  display: inline-block;
  &:before,
  &:after {
    color: ${({ theme }) => theme.colors.black};
    top: 0;
    left: 0;
    position: absolute;
  }
  &:before {
    content: '☆☆☆☆☆';
    width: 100%;
  }
  &:after {
    content: '★★★★★';
    width: ${props => props.percentage}%;
    overflow: hidden;
  }
`;
export const Title = styled.h2`
  font-size: 24px;
  line-height: 1.2em;
  margin-bottom: 5px;
`;
export const Plot = styled.div`
  font-size: 18px;
  line-height: 1.4em;
  margin-bottom: 35px; ;
`;
export const Released = styled.div`
  font-size: 16px;
`;
