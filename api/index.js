import secrets from './secrets';
import {
  resetMovies,
  updatePaginationCounter,
  fetchSearchDataBegin,
  fetchSearchDataSuccess,
  fetchSearchDataFailure,
  fetchMovieDataBegin,
  fetchMovieDataSuccess,
  fetchMovieDataFailure,
} from '../redux/actions';

export const imageExists = async (url, callback) => {
  const result = await fetch(url, { method: 'HEAD' })
    .then(data => true)
    .catch(error => false);
  callback(result);
};

export const handleErrors = response => {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
};

export const searchURL = (searchQuery, pagination) =>
  `https://www.omdbapi.com/?apikey=${secrets.apiKey}&s=${searchQuery}&page=${pagination}`;
export const movieURL = id =>
  `https://www.omdbapi.com/?apikey=${secrets.apiKey}&i=${id}`;

export const fetchData = async (store, query, pagination = 1) => {
  await fetch(searchURL(query, pagination))
    .then(store.dispatch(fetchSearchDataBegin()))
    .then(handleErrors)
    .then(response => response.json())
    .then(data => {
      store.dispatch(
        fetchSearchDataSuccess({
          response: (data.Response === 'True' && true) || false,
          message: data.Error,
          movies: data.Search || [],
          total: parseInt(data.totalResults) || 0,
        })
      );
    })
    .catch(() => {
      store.dispatch(fetchSearchDataFailure(true));
    });
};

export const getSearchData = (store, action) => {
  store.dispatch(resetMovies());
  fetchData(store, action.payload)
};

export const loadMoreSearchData = store => {
  store.dispatch(updatePaginationCounter());
  fetchData(
    store,
    store.getState().search.searchQuery,
    store.getState().search.currentPagination
  );
};

export const getMovieData = async (store, action) => {
  await fetch(movieURL(action.payload))
    .then(store.dispatch(fetchMovieDataBegin()))
    .then(handleErrors)
    .then(response => response.json())
    .then(data => {
      // Simulate slow response
      setTimeout(() => {
        store.dispatch(
          fetchMovieDataSuccess({
            content: data,
          })
        );
      }, 1000);
    })
    .catch(() => {
      store.dispatch(fetchMovieDataFailure(true));
    });
};
