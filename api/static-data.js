export default {
  pageTitle: 'Napo Movies',
  snippet:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  buttonLabel: 'Load more',
  posterImage: '/images/placeholders/placeholder-poster.jpg',
  loadingLabel: 'Loading...',
  missingTitle: 'Title unavailable',
  searchMoviePlaceholder: 'Search for a movie...',
  releasedIn: 'Released in ',
  NA: 'N/A',
  noRating: 'Rating unavailable',
};
