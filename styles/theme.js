const theme = {
  colors: {
    black              : '#000000',
    white              : '#FFFFFF',
    transparent        : '#00000000',
  },
  fonts: {
    arial               : "Arial, Helvetica, sans-serif"
  },
};

export default theme;
