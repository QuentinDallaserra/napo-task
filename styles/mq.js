const width = {
  mobile: "550px",
  tablet: "768px",
  laptop: "1024px",
  desktop: "1280px",
};

export const device = {
  mobile: `(max-width: ${width.mobile})`,
  tablet: `(max-width: ${width.tablet})`,
  laptop: `(max-width: ${width.laptop})`,
  desktop: `(max-width: ${width.desktop})`,
};
