export const removeDuplicateByKey = (items, key) =>
  items.filter(
    (tag, index, array) => array.findIndex(t => t[key] == tag[key]) == index
  );

export const lastUrlSegment = () =>
  window.location.pathname.substring(
    window.location.pathname.lastIndexOf('/') + 1
  );
